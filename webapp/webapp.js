/* 

MINIFY FUNCTIONS LIKE CONSOLE.LOG

*/


var log = function ( a ) {
	console.log ( a );
}


/* 

CREATING OBJECT WEBAPPCONTAINER
WHICH CONTAINS ALL THE SETTINGS
TO INITIALIZE THE GAME 

*/

var WebAppContainer = new function( ) {

	var me = this ;
	me.gameUrl = $( '.WebAppContainer' ).attr( 'gameUrl' ) ;
	me.gameWidth = $( '.WebAppContainer' ).attr( 'w' ) ;
	me.gameHeight = $( '.WebAppContainer' ).attr( 'h' ) ;
	me.agent = navigator.userAgent ;
	me.DOMElementsLength ;
	me.buttons = [ ] ;
	me.isMobile = function( ){
		var a = me.agent ;
		if(a.match( /iPhone/i ) || a.match( /iPad/i ) || a.match( /iPod/i ) || a.match( /Android/i ) || a.match( /Windows Phone/i )) {
		    return 1 ;
		}else{
		    return 0 ;
		}
	};
	me.mobile = me.isMobile( ) ;
	me.WebAppContent = {} ;
	me.debug = false ;

	me.calculateCss3Prefix = function( ){
		var a = me.agent ;
		if( a.match( /Firefox/i ) ) {
			var b = "" ;
		}
		if( a.match( /Safari/i ) ) {
			var b = "-webkit-" ;
		}
		if( a.match( /Windows/i ) ) {
			var b = "-ms-" ;
		}
		return b ;
		
	}
	me.css3Prefix = me.calculateCss3Prefix( navigator.userAgent );

	me.log = function( msg ) {

		if ( me.debug ) {
			var temp = $('.btn_status_bar').html() ;
			var t = new Date;
			var hh = t.getHours() ;
			var mm = t.getMinutes() ;
			var ss = t.getSeconds() ;
			$('.btn_status_bar').html( temp + '<br>' + hh + ':' + mm + ':' + ss + ' > ' + msg ) ;
			$('.btn_status_bar').scrollTop( 100000000 ) ;
		}

	}
	
	me.WebAppContainerTouchesInit = function( ) {
		/*me.DOMElementsLength = document.all.length ;

		for (var x = 0; x < me.DOMElementsLength ; x++) { // SEARCHING FOR ALL THE TOUCHABLE BUTTONS
			var d = document.all[x] ;
			var i = d.className.indexOf('btn_') ;
			if ( i == 0 ) {
				log ( d.className ) ;
				me.buttons.push({'id' : x, 'className' : d.className, 'minX' : d.offsetLeft, 'minY' : d.offsetTop, 'maxX' : d.offsetLeft + d.offsetWidth, 'maxY' : d.offsetTop + d.offsetHeight}) ; // ADDING ALL THE CLASSES THAT STARTS WITH btn_
			}
		}*/
	}
	

	me.resize = function( ) {
		var w = window.innerWidth
			,h = window.innerHeight
			,rapp = w/h
			,s;
		if ( w < WebAppContainer.WebAppContent.maxScreenWidth ) {
			if( rapp >= WebAppContainer.WebAppContent.rapp ) {
				s = h / WebAppContainer.WebAppContent.h ;
			}

			if ( WebAppContainer.WebAppContent.rapp > rapp ) {
				s = w / WebAppContainer.WebAppContent.w ;			
			}
		} else if ( h < WebAppContainer.WebAppContent.maxScreenHeight ) {
			if( rapp <= WebAppContainer.WebAppContent.rapp ) {
				s = w / WebAppContainer.WebAppContent.w ;
			}

			if ( WebAppContainer.WebAppContent.rapp < rapp ) {
				s = h / WebAppContainer.WebAppContent.h ;			
			}
		} else {
			s = 1;
		}
		x = ( w / 2 ) - ( WebAppContainer.WebAppContent.w * s / 2 ) ;
		$( '.WebAppContent' ).css( 'left',x);
		$( '.WebAppContent' ).css( WebAppContainer.css3Prefix + 'transform','scale('+s+','+s+')' ) ;
		$( document ).scrollTop(0) ;
		$( document ).scrollLeft(0) ;
		var scaleCorrectionFactor = $('.WebAppContent').css( WebAppContainer.css3Prefix + 'transform' ) ;
		var scaleCorrectionFactorvals = scaleCorrectionFactor.split( ',' ) ;
		var XscaleCorrectionFactorvals = scaleCorrectionFactorvals[3] ;
	 	WebAppContainer.WebAppContent.screenScale = XscaleCorrectionFactorvals ;
	}

	me.init = function( gameUrl ) {

		debugInterface = "";
		if ( $( '.WebAppContainer' ).attr( 'debug' ) == "true" ){
			me.debug = true ;
			var debugInterface = '<div id="pointer"></div><div class = "btn_status_bar">ready!</div><div class = "btn_reload"></div>' ;
		}

		$( '.WebAppContainer' ).append( debugInterface + '<iframe class="WebAppContent" id = "myFrame" name="myFrame" src="' + me.gameUrl + '" width="0" height="0" frameborder="0"></iframe>' ) ;
		$( '.WebAppContent' ).css( 'width', me.gameWidth );
		$( '.WebAppContent' ).css( 'height', me.gameHeight );
		me.WebAppContainerTouchesInit( ) ;
		
		$( '.btn_reload' ).bind( 'touchstart click', function( e ) {
			$( this ).addClass( 'btn_rotation' ) ;
			setTimeout( function( ) { document.location.reload( ) ; }, 500 );
		}) ;
		/*
		if ( me.mobile ) {
			document.addEventListener( 'touchstart', function(e) {
		    	me.log ('WebAppContainer Touch Event') ;
		        e.preventDefault( ) ;
		        var touch = e.touches[0] ;
		        var x = e.changedTouches[0].pageX ;
				var y = e.changedTouches[0].pageY ;
		        $('#pointer').css('left', ( x ) - 25 ) ;
		        $('#pointer').css('top', ( y ) - 25 ) ;
		        for( var i = 0 ; i < WebAppContainer.buttons.length ; i++ ) {
					var target = WebAppContainer.buttons[i] ;
				  	if ( x > target.minX && x < target.maxX && y > target.minY && y < target.maxY ) {
				    	if( target.className == "btn_reload" ) { 
				    		$('.'+target.className).addClass('btn_rotation') ;
							setTimeout(function( ){ document.location.reload( ) ; }, 500) ;
						}
				  	}
			    }
			}, false );
		} else {
			$('.btn_reload').click(function( ){
				$(this).addClass('btn_rotation');
				setTimeout( function( ) { document.location.reload( ) ; }, 500);
			})
		}
		*/

		me.WebAppContent.w = $( '.WebAppContent' ).width( ) ;
		me.WebAppContent.h = $( '.WebAppContent' ).height( ) ;
		me.WebAppContent.rapp = me.WebAppContent.w / me.WebAppContent.h ;
		me.WebAppContent.screenScale ;
		me.WebAppContent.maxScreenWidth = me.gameWidth ;
		me.WebAppContent.maxScreenHeight = me.gameHeight ;

		$( document ).ready( function ( e ) {
			me.resize( );
		});

		$( window ).resize( function ( e ) {
			me.resize( );
		});

		if( me.isMobile ){
			window.addEventListener("orientationchange", function( ) {
				me.resize( );
				me.WebAppContainerTouchesInit( ) ;
				me.log( WebAppContainer.WebAppContent.screenScale );
			}, false );
		}

		me.log( me.agent ) ;
	}
}

/* FUNCTION CALLED FROM IFRAME */

function getW( ){
	return WebAppContainer.WebAppContent.screenScale;
}

function logInDebug( a ){
	WebAppContainer.log( a );
}





