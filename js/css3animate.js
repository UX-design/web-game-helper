var Animate = function ( id, anim, delay ) {
	if ( !$( id ).hasClass( 'animated' ) ) {
		$( id ).css( 'visibility', 'hidden' ) ;
		if ( anim == '' ) anim = "fadeIn" ;
		setTimeout( function() { $( id ).css( 'visibility', 'visible' ); $( id ).addClass( anim + ' animated' ) ; }, delay );
		setTimeout( function() { $( id ).removeClass( anim + ' animated' ) ; delete( this ) ; }, delay + 700 ) ;
	}
}