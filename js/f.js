"use strict"

$(function(){


	/* 		INITIALIZE THE CELLS		*/


	var colori = [
		'sedia'
		,'sedia'
		,'bibita'
		,'popcorn'
		,'pellicola'
		,'ciak'
		,'popcorn'
		,'occhiali'
		,'megafono'
		,'megafono'
		,'occhiali'
		,'cinepresa'
		,'ciak'
		,'pellicola'
		,'cinepresa'
		,'bibita'
	]  ;


	/* 		SORT THE CELLS ORDER RANDOMLY		*/


	colori = colori.sort( function() { return .5 - Math.random() ; } );


	/*		CACHING SOME GLOBAL VARS		*/


	var agent = navigator.userAgent ;
	var windowW = window.innerWidth ;
	var windowH = window.innerHeight ;
	var myTimer = new Timer( 90, function(){  }, '.timer' ) ;
	var soundsEnabled = true ;


	/*		DISABLE SOUND EVENTS FOR SOME BROWSERS AND DEVICES		*/


	if ( agent.match( /MSIE 7/i ) 
		|| agent.match( /MSIE 8/i ) 
		|| agent.match( /MSIE 9/i ) 
		|| agent.match( /MSIE 10/i ) 
		|| agent.match( /Windows Phone/i ) ){
		var soundsEnabled = false ;
	}


	/*		MOBILE DEVICES GOES HERE		*/


	if( agent.match( /iPhone/i ) 
		|| agent.match( /iPad/i ) 
		|| agent.match( /iPod/i ) 
		|| agent.match( /Android/i ) 
		|| agent.match( /Windows Phone/i )) {
		var mobile = 1  ;
	}else{
		var mobile = 0  ;
	}


	/*		SOME VARS..		*/


	var x
		, time = 0
		, cellSelected = [ ] ;


	/*		CREATE SOUNDS OBJECTS		*/


	var soundfx = new Howl({
		urls: ['snd/soundfx.mp3','snd/soundfx.ogg','snd/soundfx.aif','snd/soundfx.wav'], 
		loop: false,
		volume: 0.8, 
		sprite: { none: [ 0, 1 ], swap: [ 0, 450 ], ding: [ 722, 1570 ], welcome: [ 2539, 1600 ], trance: [ 4400, 1994, true ], monster: [ 6580, 1320 ] }, 
		onend: function() {}
	}) ;


	/*		SOME DEBUGS..		*/


	if( !css3Support() ){
		window.parent.logInDebug( 'WARNING: NO CSS3 SUPPORT FOUND !' );
	}

	if( css3Support() ){
		window.parent.logInDebug( 'INFO: CSS3 SUPPORT FOUND !' );
	}

	if( !mobile ){
		window.parent.logInDebug( 'INFO: DESKTOP DEVICE FOUND !' );
	}

	if( mobile ){
		window.parent.logInDebug( 'INFO: MOBILE DEVICE FOUND !' );
	}


	/*		ONCE IS ALL LOADED 		*/


	$(document).ready(function(e) {
		//init()  ;
		//resize()  ;
		anima( '.logo', 'bounceInDown', 0, '' )  ;
		anima( '.title', 'bounceInDown', 300, '' )  ;	
		$( '.btn_start' ).bind( 'touchstart click', function( e ) {
			if( soundsEnabled ){ 
				soundfx.play( 'none' ) ; 
			}
			init();
			$( '.am_player' ).css( 'visibility', 'visible' ) ;
			$( this ).hide() ;
		}) ;
	}) ;

	$(window).resize(function(e) {
		resize() ;
	}) ;


	/*		SEARCH FOR CSS3 SUPPORT 		*/


	function css3Support() {
		var a = false ;
		var b = document.body.style ;
		if ( '-webkit-transform' in b ) { a = true ; }
		if ( '-moz-transform' in b ) 	{ a = true ; }
		if ( '-ms-transform' in b ) 	{ a = true ; }
		if ( '-o-transform' in b ) 		{ a = true ; }
		if ( 'transform' in b )			{ a = true ; }
		if ( agent.match( /MSIE 7/i ) || agent.match( /MSIE 8/i ) || agent.match( /MSIE 9/i ) || agent.match( /MSIE 10/i ) || agent.match( /Windows Phone/i ) ){
			a = false ;
		}
		return a ;
	}  


	/*		CSS3 ANIMATIONS 		*/

	 
	function anima( id, anim, to, fx )
	{
		$( id ).css( 'visibility', 'hidden' ) ;
		if( anim == '' ) anim = animationDefault ;
		setTimeout(function() { $( id ).css( 'visibility', 'visible' ) ; $( id ).addClass( anim + ' animated' ) ; if( soundsEnabled ){ soundfx.play( fx ) ; } }, to ) ;
		setTimeout(function() { $( id ).removeClass( anim + ' animated' ) ; }, to + 1700 ) ;
	}



	/*		SOME GLOBAL VARS FOR MESSAGES.. 		*/



	var msg1 = false ;
	var msg2 = false ;
	var msg3 = false ;


	/*		GAME ENGINE (fps is declared in the init function )		*/


	function engine()
	{
		if ( myTimer.currentTime == 60 && msg1 == false) {
			var a = new Animate( '.timer', 'shake', 10 ) ;
			var b = new Message( 'HURRY UP!', 'message', ( windowW / 2 ) - 250, ( windowH / 2 ) - 50, 500, 100, 3000, 'lightSpeedIn', 'lightSpeedOut' ) ;
			msg1 = true ;
		}
		if ( myTimer.currentTime == 30 && msg2 == false) {
			var c = new Animate( '.timer', 'shake', 10 ) ;
			var d = new Message( 'HURRY UP!', 'message', ( windowW / 2 ) - 250, ( windowH / 2 ) - 50, 500, 100, 3000, 'lightSpeedIn', 'lightSpeedOut' ) ;
			msg2 = true ;
		}
		if ( myTimer.currentTime == 0 && msg3 == false) {
			var msg_monster = new Message( '<img src = "svg/monster.svg" width = "100%" height = "100%">', 'message', ( windowW / 2 ) - 350, ( windowH / 2 ) - 250, 800, 800, 5000, 'shake', 'bounceOutDown' ) ;
			setTimeout( function() { if( soundsEnabled ){ soundfx.play( 'monster' ) ; } }, 200 );
			msg3 = true ;
		}
	}


	/*		PRELOAD IMAGES 		*/


	function preload_pictures( picture_urls, callback )
	{
		console.log(picture_urls) ;
		var loaded = 0 ;for(var i = 0, j = picture_urls.length ; i < j ; i++) {
			var img = new Image() ; img.onload = function() {
				if( ++loaded == picture_urls.length && callback ){ callback() ; }
			}
			img.src = picture_urls[ i ] ;
		}
	}


	/*		INITIALIZE THE APP 		*/


	function init()
	{

		if( soundsEnabled ){ 


		/*		PLAY THE WELCOME SOUND		*/


			soundfx.play( 'welcome' ) ; 


		/*		PLAY THE MUSIC LOOP AFTER 1 SECOND		*/


			setTimeout( function(){ if( soundsEnabled ){ soundfx.play( 'trance' ) ; } }, 1000 ) ;  

		}


		/*		THE FIRST INFO MESSAGE		*/


		var a = new Message( 'TROVA LE COPPIE!', 'message', ( windowW / 2 ) - 250, ( windowH / 2 ) - 50, 500, 100, 3000, 'lightSpeedIn', 'lightSpeedOut' ) ;


		/*		GAME TIMER AFTER 3 SECONDS		*/


		setTimeout( function() { myTimer.start() ; }, 3000 ) ;


		/*		RUN GAME ENGINE AT 10 FPS		*/


		setInterval( function() { engine() ; }, 100 ) ;


		/*		INIT THE CELL FIRST ID		*/


		var y = 1 ;


		/*		USE .SVG OR .PNG WHEN .SVG IS UN-SUPPORTED		*/


		var dir = 'svg';
		var ext = 'svg';

		if(agent.match(/MSIE/i)){
			var dir = 'img';
			var ext = 'png';
		}


		/*		CREATE THE CELLS ELEMENTS IN THE DOM AND ASSIGN PROPERTIES		*/


		do { $( '.am_player' ).append( '<div class="am_player_cell" n="' + y + '"><div class="front pos' + y + '"></div><div class="back bpos' + y + '" n="' + y + '"><img src="' + dir + '/' + colori[ y - 1 ] + '.' + ext + '" width = "100%" height = "100%"></div></div>') ; y ++ ; } while ( y < 17 ) ;


		/*		CELLS EVENTS		*/


		$( '.am_player_cell' ).bind( 'touchstart click', function( e ) {
			if( cellSelected.length < 2 ) {
				if ( css3Support() ) {
					anima( this, 'showImage', 10, 'swap' ) ;
					anima( this, 'hideImage', 1710, '' ) ;
				} else {
					var n = $(this).attr( 'n' );
					noCssAnim( 'scopri', n ) ;
				}
				var n = parseInt( $( this ).attr( 'n' ) ) ;
				cellSelected.push( n )  ;
				if( cellSelected.length == 2 ) {
					setTimeout( function() { checkPairs() ; }, 700 ) ;
				}
			}
		}) ;
	}


	/*		ANIMAZIONI PER BROWSER VECCHI		*/



	function noCssAnim( mode, n ){
		if( mode == 'scopri' ) {
			$(' .bpos' + n ).css( 'width', '0%' ) ;
			$(' .bpos' + n ).css( 'left', '50%' ) ;
			$(' .pos' + n ).css( 'left', '0%' ) ;
			$(' .pos' + n ).animate({	
				left: '50%'
				,width: '0%'
			},350) ;
			setTimeout(function(){
				$(' .bpos' + n ).animate({	
					left: '0%'
					,width: '100%'
				},350) ;
			},350);
			setTimeout( function() {
				noCssAnim( 'copri', n );
			},1700);
		}
		if( mode == 'copri' ) {
			$(' .pos' + n ).css( 'width', '0%' ) ;
			$(' .pos' + n ).css( 'left', '50%' ) ;
			$(' .bpos' + n ).css( 'left', '0%' ) ;
			$(' .bpos' + n ).animate({	
				left: '50%'
				,width: '0%'
			},350) ;
			setTimeout( function() {
				$(' .pos' + n ).animate({	
					left: '0%'
					,width: '100%'
				},350) ;
			},350);
		}
	}


	/*		CONTROL IF THE SELECTED 2 CELLS ARE SIMILAR		*/


	function checkPairs()
	{
		var a = cellSelected[0] ;
		var b = cellSelected[1] ;
		if( a != b ){
			if( colori[ a - 1 ] == colori[ b - 1 ] ){
				setTimeout(function(){ $( '.pos' + a ).parent().css( 'visibility', 'hidden' ); $( '.pos' + b ).parent().css( 'visibility', 'hidden' ) ; }, 700 );
				showStar( a, 1 ) ;
				showStar( b, 2 ) ;
				if( soundsEnabled ){ soundfx.play( 'ding' ) ; }
				window.parent.logInDebug( 'new couple found! # [' + a + '][' + b + ']' );
			}
		}
		setTimeout( function(){ cellSelected = [] ; }, 700 ) ;
	}



	/*		DEPRECATED		*/



	function whichCellIsHere( x, y )
	{
		var screenScale = window.parent.getW()  ;
		y -= ( $( '.am_player' ).position().top * screenScale ) ;
		var col = Math.ceil( x / 80 ) ;
		var row = Math.ceil( y / 80 ) ;
		var cell = ( 4 * row ) + ( col - 4 ) ;
		return cell ;
	}


	/*		SHOW THE STARS WHEN CELLS ARE PAIRS		*/


	function showStar( a, n )
	{

		if( css3Support() ){
			var x = $( '.pos' + a ).offset().left ;
			var y = $( '.pos' + a ).offset().top ;
			$( '.star' + n ).css( 'left', x ) ;
			$( '.star' + n ).css( 'top', y ) ;
			$( '.star' + n ).css( 'visibility', 'visible' ) ;
			anima( '.star' + n , 'bounceIn', 10, '' ) ;
			anima( '.star' + n , 'fadeOutUp', 2000, '' ) ;
			setTimeout( function() {
				$( '.star' + n ).css( 'visibility', 'hidden' ) ;
				$( '.pos' + a ).parent().css( 'visibility', 'hidden' ) ; 
			}, 2700 ) ;
		}else{
			var cellWidth = $( '.am_player_cell' ).width() ;
			var x = $( '.pos' + a ).offset().left - ( cellWidth / 2 ) ;
			var y = $( '.pos' + a ).offset().top;
			$( '.star' + n ).css( 'left', x ) ;
			$( '.star' + n ).css( 'top', y ) ;
			$( '.star' + n ).css( 'visibility', 'visible' ) ;
			setTimeout( function() {
				$( '.star' + n ).animate({
					width: '0%',
					height: '0%',
					left: x + ( cellWidth / 2 ),
					top: y + ( cellWidth / 2 ),
				},300);
			},1000);
			setTimeout( function() {
				$( '.star' + n ).css( 'visibility', 'hidden' ) ;
				$( '.star' + n ).css( 'width', 160 ) ;
				$( '.star' + n ).css( 'height', 160 ) ; 
				$( '.pos' + a ).parent().css( 'visibility', 'hidden' );
			}, 2700 ) ;
		}
	}


	/*		PREVENT SCROLLING OF THE PAGE ON MOBILE DEVICES		*/


	document.ontouchstart = function(e){ 
	    e.preventDefault(); 
	}

});
