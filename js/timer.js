"use strict"


var Timer = function ( duration, callback, elementId ) {
	var me = this ;
	me.duration = duration ;
	me.currentTime = duration ;
	me.elementId = elementId ;
	me.timer = false ;
	me.callback = function() { callback() ; }
}

Timer.prototype.currentTime = 0;

Timer.prototype.start = function() { 
	var me = this ;
	if( !me.timer ) {

	}
	me.timer = setInterval( function() {
		if( me.elementId ){
			me.currentTime -= 1 ;
			$( me.elementId ).html( me.currentTime ) ;
		}
		if( me.currentTime == 0 ) {
			me.stop() ;
		}
	}, 1000 ) ; 
}

Timer.prototype.stop = function() {
	var me = this ;
	clearInterval( me.timer ) ;
	me.timer = false ;
	me.callback() ;
}