"use strict"

var messageIsShowing = false ;

var Message = function ( testo, classname, x, y, w, h, duration, animIn, animOut ) {
	messageIsShowing = true ;
	var me = this ;
	me.x = x ;
	me.y = y ;
	me.w = w ;
	me.h = h ;
	me.testo = testo ;
	me.duration = duration ;
	me.classname = classname ;
	me.messageIsShowing = true ;
	$( 'body' ).append( '<div class = "' + me.classname + '" style = "left: ' + me.x + 'px ; top: ' + me.y + 'px ; width: ' + me.w + 'px ; height: ' + me.h + 'px ;">' + me.testo + '</div>' ) ;
	var a = new Animate( '.message', animIn, 10 ) ;
	var b = new Animate( '.message', animOut, me.duration + 700 ) ;
	setTimeout( function() { $( '.' + me.classname ).remove() ; messageIsShowing = false ; } , me.duration + 1400 ) ;
}
